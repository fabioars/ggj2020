﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour
{
    public delegate void Broken();
    public static event Broken onBroken;

    public delegate void Repair();
    public static event Repair onRepair;

    public delegate void FirstRepair();
    public static event FirstRepair onFirstRepair;

    private bool firstRepair = false;

    [Range(0f, 100f)]
    public float maxDamage;

    public float Damage { get; private set; }

    [Range(0f, 1f)]
    public float autobreakInterval;
    [Range(0f, 10f)]
    public float autobreakAmmount;

    private Coroutine autoBreakCoroutine;
    private SpriteRenderer sprite;

    public GameObject fixParticle;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        Break();
        StartAutoBreak();
        
        if(onBroken != null)
            onBroken();
    }

    public bool IsBroken()
    {
        return Damage >= 0;
    }

    public bool IsFixed()
    {
        return Damage <= 0;
    }

    void Update()
    {
        // sprite.color = Color.Lerp(Color.white, Color.red, Damage/maxDamage);
    }

    public void Fix(float amount)
    {
        Damage -= amount;

        if(fixParticle != null)
        {
            Instantiate(fixParticle, transform.position, Quaternion.identity);
        }

        if (Damage <= 0)
        {
            Damage = 0;
            StopCoroutine(autoBreakCoroutine);
            AudioManager.instance.Play("fixed");

            if(onFirstRepair != null)
            {
                onFirstRepair();
                onFirstRepair = null;
            }

            if(onRepair != null)
            {
                onRepair();
            }

            Destroy(gameObject);
        }
    }

    public void Break()
    {
        Damage = maxDamage;
    }

    public void Break(float amount)
    {
        Damage = (Damage + amount >= maxDamage) ? maxDamage : Damage + amount;
    }

    public void StartAutoBreak()
    {
        autoBreakCoroutine = StartCoroutine(Autobreak());
    }

    IEnumerator Autobreak()
    {
        for(; ;)
        {
            yield return new WaitForSeconds(autobreakInterval);
            Break(autobreakAmmount);
        }
    }
}
