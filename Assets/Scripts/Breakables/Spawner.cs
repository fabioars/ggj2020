﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] prefabs;

    public void Spawn()
    {
        int chosen = Random.Range(0, prefabs.Length);
        
        GameObject gameObject = Instantiate(prefabs[chosen], this.transform);
        gameObject.transform.position = this.transform.position;
    }

    public bool HasChild()
    {
        return transform.childCount > 0;
    }
}
