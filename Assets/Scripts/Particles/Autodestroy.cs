﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Autodestroy : MonoBehaviour
{
    [Range(0f, 1f)]
    public float delay;

    void Start()
    {
        Destroy(gameObject, delay);
    }
}
