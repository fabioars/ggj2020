﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    [SerializeField]
    public GameObject item;

    public Sprite closed;
    public Sprite opened;

    private List<GameObject> slots;

    void Awake()
    {
        slots = new List<GameObject>(2);
    }

    void Start()
    {
        slots.Add(item);
        slots.Add(item);
        GetComponent<SpriteRenderer>().sprite = opened;
    }

    public GameObject RemoveItem()
    {
        if(IsEmpty())
        {
            throw new Exception("Não tem nada no baú");
        }

        slots.RemoveAt(0);

        if(IsEmpty())
        {
            GetComponent<SpriteRenderer>().sprite = closed;
        }

        return item;
    }

    public bool IsEmpty()
    {
        return slots.Count == 0;
    }
}
