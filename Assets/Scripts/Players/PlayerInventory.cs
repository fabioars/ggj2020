﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public delegate void GrabHammer();
    public static event GrabHammer onGrabHammer;

    public CharacterController2D controller;
    public LayerMask chestLayer;
    public Animator animator;
    public GameObject handItem;

    private List<GameObject> slots;
    public int size = 2;

    void Awake()
    {
        slots = new List<GameObject>(size);
        animator = GetComponent<Animator>();
    }

    void OnAction()
    {
        Vector2 lastMoviment = controller.m_FacingRight ? Vector2.right : Vector2.left;

        RaycastHit2D chestHitInfo = Physics2D.Raycast(transform.position, lastMoviment, 2f, chestLayer);
        if (chestHitInfo.collider != null)
        {
            GameObject chestGameObject = chestHitInfo.collider.gameObject;
            Chest chest = chestGameObject.GetComponent<Chest>();
            animator.SetTrigger("interact");
            StartCoroutine(GameCrontroller.instance.CountDownCoroutine());

            try
            {
                if (!IsFull())
                {
                    AudioManager.instance.Play("chest_item");
                    GameObject chestItem = chest.RemoveItem();
                    Add(chestItem);
                }
                else
                {
                    Debug.Log("Inventário cheio");
                }
            } catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    public GameObject Get(int position)
    {
        return slots[position];
    }

    public void Add(GameObject item)
    {
        if(!IsFull())
        {
            slots.Add(item);
            handItem.SetActive(true);

            if(onGrabHammer != null)
            {
                onGrabHammer();
            }
        }
        else
        {
            throw new Exception("Inventário está cheio");
        }
    }

    public GameObject Remove(int position)
    {
        GameObject item = slots[position];
        slots.RemoveAt(position);
        return item;
    }

    public GameObject Remove(GameObject item)
    {
        slots.Remove(item);
        return item;
    }

    public GameObject[] GetItemList()
    {
        return slots.ToArray();
    }

    public bool IsFull()
    {
        return slots.Count >= size;
    }

    public bool IsEmpty()
    {
        return slots.Count == 0;
    }
}
