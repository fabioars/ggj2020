﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public CharacterController2D controller;
    public PlayerInventory inventory;
    public float speed = 1f;
    public float fixAmount = 5f;
    public LayerMask ladderLayer;
    public LayerMask fixableLayer;

    private Vector2 movimentDirection;
    private bool isJumping;
    private float horizontalSpeed;
    private bool isClimbing;
    private bool isRunning;
    public Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
        inventory = GetComponent<PlayerInventory>();
    }

    void Start()
    {
        AudioManager.instance.Play("player_in");
    }

    void FixedUpdate()
    {
        RaycastHit2D ladderHitInfo = Physics2D.Raycast(transform.position, Vector2.up, .2f, ladderLayer);
        if (ladderHitInfo.collider != null)
        {
            isClimbing = movimentDirection.y > 0 || movimentDirection.y < 0;
        }
        else
        {
            isClimbing = false;
        }

        float runningMultiplier = isRunning ? 2f : 1f;

        Vector2 moviment = new Vector2(horizontalSpeed * runningMultiplier * Time.fixedDeltaTime, movimentDirection.y);
        controller.Move(moviment, false, isJumping, isClimbing);

        animator.SetBool("isMoving", movimentDirection.x > 0 || movimentDirection.x < 0);
        animator.SetBool("isJumping", isJumping);
        animator.SetBool("isClimbing", isClimbing);
    }

    void OnMove(InputValue input)
    {
        movimentDirection = input.Get<Vector2>();
        horizontalSpeed = movimentDirection.x * speed;
    }

    void OnJump(InputValue jumpingStatus)
    {
        isJumping = jumpingStatus.Get<float>() > 0;

    }

    void OnRun(InputValue jumpingStatus)
    {
        isRunning = jumpingStatus.Get<float>() > 0;
    }

    void OnAction()
    {
        Vector2 lastMoviment = controller.m_FacingRight ? Vector2.right : Vector2.left;

        RaycastHit2D ladderHitInfo = Physics2D.Raycast(transform.position, lastMoviment, 2f, fixableLayer);
        if (ladderHitInfo.collider != null && inventory.IsFull())
        {
            GameObject fixableObject = ladderHitInfo.collider.gameObject;

            Breakable item = fixableObject.GetComponent<Breakable>();
            if(!item.IsFixed())
            {
                item.Fix(fixAmount);

                AudioManager.instance.Play("fix");
                animator.SetTrigger("interact");
            }
        }
    }
}
