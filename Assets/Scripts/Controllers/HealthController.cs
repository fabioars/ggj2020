﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    public delegate void HighDanger();
    public static event HighDanger onHighDanger;
    
    public static HealthController instance;

    public int HealthShip = maxHealthShip;
    
    public static int maxHealthShip = 1000;

    private int shipDamage = 0;
    private bool isLowDamaged = true;
    [Range(0f, 10f)]
    private float intervalToleranceDamage = 3f;

    private Coroutine decreaseHealthCoroutine;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void OnEnable()
    {
        Breakable.onBroken += DecreaseHealth;
        Breakable.onRepair += IncreaseHealth;
    }

    void OnDisable()
    {
        Breakable.onBroken -= DecreaseHealth;
        Breakable.onRepair -= IncreaseHealth;
    }

    void Start()
    {
        decreaseHealthCoroutine = StartCoroutine(DecreaseHealthCoroutine());
    }

    public void Finish()
    {
        StopCoroutine(decreaseHealthCoroutine);
    }


    void DecreaseHealth()
    {
        shipDamage += 10;
    }

    void IncreaseHealth()
    {
        shipDamage -= 100;

        if(shipDamage < 0)
            shipDamage = 0;
    }

    IEnumerator DecreaseHealthCoroutine()
    {
        for(; ;)
        {
            yield return new WaitForSeconds(intervalToleranceDamage);

            if(isLowDamaged && ((HealthShip * 2) <= maxHealthShip) && (onHighDanger != null))
            {
                onHighDanger();
                isLowDamaged = false;
            }

            if(HealthShip > 0)
            {
                HealthShip -= shipDamage;
            }
            else
            {
                GameCrontroller.instance.Finish();
            }
        }
    }
}
