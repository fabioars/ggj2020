﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsController : MonoBehaviour
{
    public static ItemsController instance;

    public Spawner[] Spawners;
    // public Collectable[] collectables;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Finish()
    {
    }
}
