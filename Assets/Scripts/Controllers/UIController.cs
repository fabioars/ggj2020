﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{ 
    public static UIController instance;

    public GameObject PlayPanel;

    public GameObject OverPanel;
    
    public GameObject HealthPanel;
    public Slider healthSlider;
    public Image healthFill;
    public Sprite sprite;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void OnEnable()
    {
        PlayPanel.SetActive(false);
        OverPanel.SetActive(false);
        HealthPanel.SetActive(false);

        GameCrontroller.onGameStart += DisplayGamePlay;
        GameCrontroller.onGameOver += DisplayGameOver;
        HealthController.onHighDanger += ReplaceFill;
    }

    void OnDisable()
    {
        GameCrontroller.onGameStart -= DisplayGamePlay;
        GameCrontroller.onGameOver -= DisplayGameOver;
        HealthController.onHighDanger -= ReplaceFill;
    }

    void DisplayGamePlay()
    {
        OverPanel.SetActive(false);
        HealthPanel.SetActive(false);
        PlayPanel.SetActive(true);

        StartCoroutine(Delay());
    }

    void DisplayGameOver()
    {
        PlayPanel.SetActive(false);
        HealthPanel.SetActive(true);
        OverPanel.SetActive(true);
    }

    void DisplayGameHealth()
    {
        PlayPanel.SetActive(false);
        OverPanel.SetActive(false);
        HealthPanel.SetActive(true);
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(2f);

        DisplayGameHealth();
    }

    void FixedUpdate()
    {
        healthSlider.value = (HealthController.instance.HealthShip / (float)HealthController.maxHealthShip);
    }

    void ReplaceFill()
    {
        healthFill.sprite = sprite;
    }

    public void Finish()
    {
    }
}
