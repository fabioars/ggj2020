﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempestController : MonoBehaviour
{
    public static TempestController instance;

    public GameObject background;
    public ParticleSystem rain;

    public Sprite backgroundTempest;

    private SpriteRenderer render;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void OnEnable()
    {
        render = background.GetComponent<SpriteRenderer>();

        GameCrontroller.onGameStart += MakeTempest;
    }

    void OnDisable()
    {
        GameCrontroller.onGameStart -= MakeTempest;
    }

    void MakeTempest()
    {
        render.sprite = backgroundTempest;
        rain.enableEmission = true;
        rain.Play();

        AudioManager.instance.Stop("main-song");
        AudioManager.instance.Play("storm-song");
    }

    public void Finish()
    {
        AudioManager.instance.Play("main-song");
        AudioManager.instance.Stop("storm-song");

        rain.Stop();
    }
}
