﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameCrontroller : MonoBehaviour
{
    public static GameCrontroller instance;

    public delegate void GameStart();
    public static event GameStart onGameStart;

    public delegate void GameOver();
    public static event GameOver onGameOver;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start() {
        AudioManager.instance.Play("main-song");
    }
    void OnEnable()
    {
        PlayerInventory.onGrabHammer += Play;
    }

    void OnDisable()
    {
        PlayerInventory.onGrabHammer -= Play;
    }

    void Play()
    {
        if(onGameStart != null)
        {
            onGameStart();
            onGameStart = null;
        }
    }

    public void Finish()
    {
        HealthController.instance.Finish();
        ItemsController.instance.Finish();
        UIController.instance.Finish();
        BreakablesController.instance.Finish();
        TempestController.instance.Finish();

        if(onGameOver != null)
            onGameOver();

        Time.timeScale = 0f;
    }

    public IEnumerator CountDownCoroutine()
    {
        yield return new WaitForSeconds(120f);
        Finish();
        yield return new WaitForSecondsRealtime(5f);
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
