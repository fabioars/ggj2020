﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakablesController : MonoBehaviour
{
    public static BreakablesController instance;

    public int maxSpawnsPerTurn = 2;

    [Range(0, 30)]
    public int intervalBetweenTurns = 20;

    [Range(0f, 1f)]
    public float chanceToSpawn = 0.4f;
    public float intervalBetweenSpawn = 0.5f; 

    private static Spawner[] spawners;
    private Coroutine takeTurnsCoroutine;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    void OnEnable()
    {
        spawners = ItemsController.instance.Spawners;
        GameCrontroller.onGameStart += FirstBreakableSpawn;
        Breakable.onFirstRepair += TakeTurns;
    }

    void OnDisable()
    {
        GameCrontroller.onGameStart -= FirstBreakableSpawn;
        Breakable.onFirstRepair -= TakeTurns;
    }

    void TakeTurns()
    {
        takeTurnsCoroutine = StartCoroutine(TakeTurnsCoroutine());
    }

    public void Finish()
    {
    }

    void FirstBreakableSpawn()
    {
        spawners[0].Spawn();
    }

    IEnumerator TakeTurnsCoroutine()
    {
        for(; ;)
        {
            int countSpawns = 0;
            foreach(Spawner spawner in spawners)
            {
                float randn =  Random.Range(0f, 1f);

                if(randn < chanceToSpawn && !spawner.HasChild() && countSpawns < maxSpawnsPerTurn)
                {
                    spawner.Spawn();
                    countSpawns++;
                }

                yield return new WaitForSeconds(intervalBetweenSpawn);
            }

            yield return new WaitForSeconds(intervalBetweenTurns);
        }
    }
}
